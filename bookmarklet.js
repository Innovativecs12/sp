
	var bodyWords = (function(){

		var sWords = document.body.innerText.toLowerCase().trim().replace(/[,;.]/g,'').split(/[\s\/]+/g).sort();
		var iWordsCount = sWords.length;

		var counts = {};
		for (var i=0; i<iWordsCount; i++) {
			var sWord = sWords[i];
				counts[sWord] = counts[sWord] || 0;
				counts[sWord]++;
		}

		var wordArray = [];
		for (sWord in counts) {
			wordArray.push({
				text: sWord,
				frequency: counts[sWord]
			});
		}

		return wordArray.sort(function(a,b){
			return (a.frequency > b.frequency) ? -1 : ((a.frequency < b.frequency) ? 1 : 0);
		});

	}());

	(function(){
		var iWordsCount = bodyWords.length;

		for (var i=0; i<iWordsCount; i++) {
			var word = bodyWords[i];

			var reg = new RegExp('\\b'+word.text+'\\b', 'ig');

			var hasNumber = /\d/;
			hasNumber.test(word.text); // false

			$('body').append('<ul id="wordList"></ul>');

			var wordList = $('#wordList');

			if (!hasNumber.test(word.text) && word.text.length > 4) {
				var fontWeight = word.frequency.toString().concat('0');
				var dateSpan = document.createElement('span');
				dateSpan.innerHTML = word.text;
				dateSpan.style.fontSize = fontWeight+'px';
				//var span = '<span style="font-weight:'+fontWeight+'">'+word.text+'</span>';
				var replaced = $("body").html().replace(reg, dateSpan.outerHTML);
				$("body").html(replaced);
				$('#wordList').append('<li>'+word.text+'('+ word.frequency+')</li>');
			}


		}
	}());
